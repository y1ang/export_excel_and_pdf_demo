package com.jay.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

/**
 * 模板填充导出excel
 * @author 01375894
 *
 */
public class ExportExcelByTempleteUtil {

	public static void createExcel(String tplPath, String descPath, Map<String, Object> params) {
		XLSTransformer transformer = new XLSTransformer();
		InputStream is = null;
		OutputStream desc = null;
		try {
			is = new FileInputStream(tplPath);
			desc = new FileOutputStream(descPath);
			Workbook wb = transformer.transformXLS(is, params);
			wb.write(desc);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParsePropertyException | InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void createExcel(InputStream tpl, String descPath, Map<String, Object> params) {
		XLSTransformer transformer = new XLSTransformer();
		OutputStream desc = null;
		try {
			desc = new FileOutputStream(descPath);
			Workbook wb = transformer.transformXLS(tpl, params);
			wb.write(desc);
		} catch (ParsePropertyException | InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
